# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 20:26:25 2017

@author: Borivoje
"""

def sobel(img):
    dX = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3) # dx
    dY = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3) # dy
    absdX = cv2.convertScaleAbs(dX)
    absdY = cv2.convertScaleAbs(dY)
    g = cv2.addWeighted(absdX,0.5,absdY,0.5,0)
    a = cv2.phase(dX,dY,angleInDegrees=False)
    return dX, dY, g, a 