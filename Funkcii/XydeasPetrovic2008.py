# -*- coding: utf-8 -*-
"""
Created on Fri Oct 20 02:55:28 2017

@author: Tasikj
"""
from __future__ import division
import cv2
import numpy as np
from math import pi

def XydeasPetrovic20081(im1,im2,imF):
    
    # read: under, overexposed and fused images 
    un = cv2.imread(im1)
    ov = cv2.imread(im2)
    fu = cv2.imread(imF) 

    # constants for sigmoid function shape
    L = 1.5
   
    GAMMAg = 0.9994
    k_g = 15
    sigma_g = 0.5
    
    GAMMAa = 0.9879
    k_a = 22
    sigma_a = 0.8    

    # convert RGB to gray
    un = cv2.cvtColor(un, cv2.COLOR_RGB2GRAY)
    ov = cv2.cvtColor(ov, cv2.COLOR_RGB2GRAY)
    fu = cv2.cvtColor(fu, cv2.COLOR_RGB2GRAY)
   
    # removing noise (usefull for Sobel)
    un = cv2.GaussianBlur(un,(3,3),0)
    ov = cv2.GaussianBlur(ov,(3,3),0)
    fu = cv2.GaussianBlur(fu,(3,3),0)
        
    # to double
    un = np.double(un); ov = np.double(ov); fu = np.double(fu)
    
    # applied Sobel filtering
    sAx, sAy, gA, alfaA = sobel(un)
    sBx, sBy, gB, alfaB = sobel(ov)
    sFx, sFy, gF, alfaF = sobel(fu)
    
   
   # relative strength and orientation values (of A with respect to F)
    G_AF = np.zeros(un.shape,dtype=np.float64) 

    for i in range(0,un.shape[0]):
        for j in range(0,un.shape[1]):
            if(gA[i,j] > gF[i,j]):
                G_AF[i,j] = gF[i,j] / (gA[i,j] + 0.00000000001)
            else:
                G_AF[i,j] = gA[i,j] / (gF[i,j] + 0.00000000001)
                
    A_AF = (abs(abs(alfaA - alfaF)-(pi/2)))/(pi/2)
    
    Q_g_AF = GAMMAg / (1 + np.exp(-k_g*(G_AF-sigma_g)))
    Q_a_AF = GAMMAa / (1 + np.exp(-k_a*(A_AF-sigma_a)))
    Q_AF = Q_g_AF *  Q_a_AF
    
    wA = np.power(gA,L)
    

    # relative strength and orientation values (of B with respect to F)
    G_BF = np.zeros(ov.shape,dtype=np.float64)
    
    for i in range(0,ov.shape[0]):
        for j in range(0,ov.shape[1]):
            if(gB[i,j] > gF[i,j]):
                G_BF[i,j] = gF[i,j] / (gB[i,j] + 0.00000000001)
            else:
                G_BF[i,j] = gB[i,j] / (gF[i,j] + 0.00000000001)
            
    B_BF = (abs(abs(alfaB - alfaF)-(pi/2)))/(pi/2)
    
    Q_g_BF = GAMMAg / (1 + np.exp(-k_g*(G_BF-sigma_g)))
    Q_a_BF = GAMMAa / (1 + np.exp(-k_a*(B_BF-sigma_a)))
    Q_BF = Q_g_BF *  Q_a_BF
    wB = np.power(gB,L)
    
    # normalized weighted performance metric 
    sum1 = Q_AF*wA + Q_BF*wB
    sum2 = wA+wB
    
    br = np.sum(sum1)
    im = np.sum(sum2)
    
    Qm1=br/im
            
    return np.float64(Qm1)





